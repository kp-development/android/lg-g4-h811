#!/system/bin/sh
#!/sbin/busybox/sh
#!/sbin/busybox sh

# Tweak our gov
su -c echo 85 > /sys/devices/system/cpu/cpufreq/bioshock/down_threshold
su -c echo 15 > /sys/devices/system/cpu/cpufreq/bioshock/freq_step
su -c echo 1 > /sys/devices/system/cpu/cpufreq/bioshock/ignore_nice_load
su -c echo 1 /sys/devices/system/cpu/cpufreq/bioshock/sampling_down_factor
su -c echo 60000 > /sys/devices/system/cpu/cpufreq/bioshock/sampling_rate
su -c echo 30000 > /sys/devices/system/cpu/cpufreq/bioshock/sampling_rate_min
su -c echo 80 > /sys/devices/system/cpu/cpufreq/bioshock/up_threshold

# Touch Boost Tweaks
su -c echo 0:960000 1:0 2:0 3:0 4:0 5:0 > /sys/module/cpu_boost/parameters/input_boost_freq
su -c echo 0 > /sys/module/cpu_boost/parameters/boost_ms
su -c echo 50 > /sys/module/cpu_boost/parameters/input_boost_ms
su -c echo 0 > /sys/module/msm_performance/parameters/touchboost
su -c echo 1 > /sys/module/msm_thermal/core_control/enabled
su -c echo N > /sys/module/msm_thermal/parameters/enabled
