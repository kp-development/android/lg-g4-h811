#!/system/bin/sh
#!/sbin/busybox/sh
#!/sbin/busybox sh

# Tweak our gov
if [ -d /sys/devices/system/cpu/cpu0/cpufreq/ironactive ]; then
	CT=0
	while [ $CT -le 3 ]; do
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/above_hispeed_delay
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/boost
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/boostpulse_duration
		su -c echo 300 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/go_hispeed_load
		su -c echo 384000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/hispeed_freq
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/io_is_busy
		su -c echo 80000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/min_sample_time
		su -c echo "1 384000:32 400000:34 600000:40 700000:44 800000:49 900000:55 1100000:64 1400000:79 1700000:90 1900000:99" > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/target_loads
		su -c echo 60000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/timer_rate
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/timer_slack
		((CT++))
	done
fi;
if [ -d /sys/devices/system/cpu/cpu4/cpufreq/ironactive ]; then
	CT=4
	while [ $CT -le 5 ]; do
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/above_hispeed_delay
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/boost
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/boostpulse_duration
		su -c echo 300 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/go_hispeed_load
		su -c echo 384000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/hispeed_freq
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/io_is_busy
		su -c echo 80000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/min_sample_time
		su -c echo "1 384000:32 400000:34 600000:40 700000:44 800000:49 900000:55 1100000:64 1400000:79 1700000:90 1900000:99" > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/target_loads
		su -c echo 60000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/timer_rate
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ironactive/timer_slack
		((CT++))
	done
fi;

# Touch Boost Tweaks
su -c echo 0:960000 1:0 2:0 3:0 4:0 5:0 > /sys/module/cpu_boost/parameters/input_boost_freq
su -c echo 0 > /sys/module/cpu_boost/parameters/boost_ms
su -c echo 50 > /sys/module/cpu_boost/parameters/input_boost_ms
su -c echo 0 > /sys/module/msm_performance/parameters/touchboost
su -c echo 1 > /sys/module/msm_thermal/core_control/enabled
su -c echo N > /sys/module/msm_thermal/parameters/enabled

