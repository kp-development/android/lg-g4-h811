#!/system/bin/sh
#!/sbin/busybox/sh
#!/sbin/busybox sh

# Tweak our gov

su -c echo 0:960000 1:960000 2:960000 3:960000 4:960000 5:960000 > /sys/module/cpu_boost/parameters/input_boost_freq
su -c echo 0 > /sys/module/cpu_boost/parameters/boost_ms
su -c echo 50 > /sys/module/cpu_boost/parameters/input_boost_ms
su -c echo 0 > /sys/module/msm_performance/parameters/touchboost
su -c echo 1 > /sys/module/msm_thermal/core_control/enabled
su -c echo N > /sys/module/msm_thermal/parameters/enabled

