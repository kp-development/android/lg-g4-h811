#!/system/bin/sh
#!/sbin/busybox/sh
#!/sbin/busybox sh

# Tweak our gov
if [ -d /sys/devices/system/cpu/cpu0/cpufreq/zzmoove ]; then
	CT=0
	while [ $CT -le 3 ]; do
		su -c echo 80000 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/sampling_rate
		su -c echo 40000 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/sampling_rate_min
		su -c echo 80 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/afs_threshold3
		su -c echo 95 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/afs_threshold4
		su -c echo 50 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/down_threshold
		su -c echo 90 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/up_threshold
	((CT++))
	done
fi;
if [ -d /sys/devices/system/cpu/cpu4/cpufreq/zzmoove ]; then
	CT=4
	while [ $CT -le 5 ]; do
		su -c echo 80000 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/sampling_rate
		su -c echo 40000 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/sampling_rate_min
		su -c echo 80 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/afs_threshold3
		su -c echo 95 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/afs_threshold4
		su -c echo 50 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/down_threshold
		su -c echo 90 > /sys/devices/system/cpu/cpu$CT/cpufreq/zzmoove/up_threshold
	((CT++))
	done
fi;

su -c echo 0:960000 1:0 2:0 3:0 4:0 5:0 > /sys/module/cpu_boost/parameters/input_boost_freq
su -c echo 0 > /sys/module/cpu_boost/parameters/boost_ms
su -c echo 50 > /sys/module/cpu_boost/parameters/input_boost_ms
su -c echo 0 > /sys/module/msm_performance/parameters/touchboost
su -c echo 1 > /sys/module/msm_thermal/core_control/enabled
su -c echo N > /sys/module/msm_thermal/parameters/enabled
