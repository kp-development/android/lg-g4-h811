#!/system/bin/sh
#!/sbin/busybox/sh
#!/sbin/busybox sh

# Tweak our gov
if [ -d /sys/devices/system/cpu/cpu0/cpufreq/elementalx ]; then
	CT=0
	while [ $CT -le 3 ]; do
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/powersave
		su -c echo 70 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/sampling_down_factor
		su -c echo 80000 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/sampling_rate
		su -c echo 40000 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/sampling_rate_min
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/interactive/up_threshold
	((CT++))
	done
fi;
if [ -d /sys/devices/system/cpu/cpu4/cpufreq/elementalx ]; then
	CT=4
	while [ $CT -le 5 ]; do
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/powersave
		su -c echo 70 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/sampling_down_factor
		su -c echo 80000 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/sampling_rate
		su -c echo 40000 > /sys/devices/system/cpu/cpu$CT/cpufreq/elementalx/sampling_rate_min
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/interactive/up_threshold
	((CT++))
	done
fi;

su -c echo 0:960000 1:0 2:0 3:0 4:0 5:0 > /sys/module/cpu_boost/parameters/input_boost_freq
su -c echo 0 > /sys/module/cpu_boost/parameters/boost_ms
su -c echo 50 > /sys/module/cpu_boost/parameters/input_boost_ms
su -c echo 0 > /sys/module/msm_performance/parameters/touchboost
su -c echo 1 > /sys/module/msm_thermal/core_control/enabled
su -c echo N > /sys/module/msm_thermal/parameters/enabled
