#!/system/bin/sh
#!/sbin/busybox/sh
#!/sbin/busybox sh

# Tweak our gov
if [ -d /sys/devices/system/cpu/cpu0/cpufreq/ondemand ]; then
	CT=0
	while [ $CT -le 3 ]; do
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/ignore_nice_load
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/io_is_busy
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/powersave_bias
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/sampling_down_factor
		su -c echo 60000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/sampling_rate
		su -c echo 30000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/sampling_rate_min
		su -c echo 75 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/up_threshold
	((CT++))
	done
fi;
if [ -d /sys/devices/system/cpu/cpu4/cpufreq/ondemand ]; then
	CT=4
	while [ $CT -le 5 ]; do
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/ignore_nice_load
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/io_is_busy
		su -c echo 0 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/powersave_bias
		su -c echo 1 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/sampling_down_factor
		su -c echo 60000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/sampling_rate
		su -c echo 30000 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/sampling_rate_min
		su -c echo 75 > /sys/devices/system/cpu/cpu$CT/cpufreq/ondemand/up_threshold
	((CT++))
	done
fi;

# Touch Boost Tweaks
su -c echo 0:960000 1:0 2:0 3:0 4:0 5:0 > /sys/module/cpu_boost/parameters/input_boost_freq
su -c echo 0 > /sys/module/cpu_boost/parameters/boost_ms
su -c echo 50 > /sys/module/cpu_boost/parameters/input_boost_ms
su -c echo 0 > /sys/module/msm_performance/parameters/touchboost
su -c echo 1 > /sys/module/msm_thermal/core_control/enabled
su -c echo N > /sys/module/msm_thermal/parameters/enabled





